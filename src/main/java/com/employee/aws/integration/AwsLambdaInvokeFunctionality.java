package com.employee.aws.integration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.employee.entity.EmployeeAndStudent;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import java.nio.charset.Charset;

@Component
public class AwsLambdaInvokeFunctionality {

    @Value(value = "${awsAccessKey}")
    private String awsAccessKey;
    @Value(value = "${awsSecretAccessKey}")
    private String awsSecretAccessKey;

    public String invokeAwsLambda(String functionName, EmployeeAndStudent employeeAndStudent) {



        AWSCredentials credentials = new BasicAWSCredentials(awsAccessKey, awsSecretAccessKey);


        //This will convert object to JSON String
        String inputJSON = new Gson().toJson("");

        InvokeRequest lmbRequest = new InvokeRequest()
                .withFunctionName("learn-lambda-aws-realtime")
                .withPayload(inputJSON);

        lmbRequest.setInvocationType(InvocationType.RequestResponse);

        AWSLambda lambda = AWSLambdaClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(credentials)).build();

        InvokeResult lmbResult = lambda.invoke(lmbRequest);

        String resultJSON = new String(lmbResult.getPayload().array(), Charset.forName("UTF-8"));

        System.out.println(resultJSON);

        return resultJSON;
    }
}
