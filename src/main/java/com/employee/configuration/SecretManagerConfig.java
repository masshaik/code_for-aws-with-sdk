package com.employee.configuration;


import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.*;
import com.employee.dto.AwsSecrets;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
public class SecretManagerConfig {

    @Value(value = "${awsAccessKey}")
    private String awsAccessKey;

    @Value(value = "${awsSecretAccessKey}")
    private String awsSecretAccessKey;

    private Gson gson = new Gson();


    @Bean
    public DataSource dataSource() {
        AwsSecrets awsSecrets = getSecret();
        System.out.println("jdbc:" + awsSecrets.getEngine() + "://" + awsSecrets.getHost() + ":" + awsSecrets.getPort() + "/shaiktechie");
        return DataSourceBuilder.create()
                .password(awsSecrets.getPassword())
                .username(awsSecrets.getUsername())
                .url("jdbc:mysql://javatechiedb.cpvjbvlgvfvs.us-east-1.rds.amazonaws.com:3306/shaiktechie")
                .build();
    }

    private AwsSecrets getSecret() throws DecryptionFailureException {

        String secretName = "shaiklearn_db_credentials";
        String region = "us-east-1";


        // Create a Secrets Manager client
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretAccessKey)))
                .build();


        String secret, decodedBinarySecret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = null;

        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (Exception e) {
            throw e;
        }

        if (getSecretValueResult.getSecretString() != null) {
            secret = getSecretValueResult.getSecretString();
            return gson.fromJson(secret, AwsSecrets.class);
        }
        return null;

    }
}
