package com.employee.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.employee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class EmployeeRepository {


    @Autowired
    private DynamoDBMapper dynamoDBMapper;


    public Employee createEmployee(Employee employee){
         dynamoDBMapper.save(employee);
         return employee;
    }


    public Employee loadEmployee(String id){
        return dynamoDBMapper.load(Employee.class,id);
    }

    public String deleteEmployee(Employee employee){
         dynamoDBMapper.delete(employee);
         return "deleted";
    }


}
