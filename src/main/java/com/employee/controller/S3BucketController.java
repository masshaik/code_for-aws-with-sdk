package com.employee.controller;

import com.employee.service.BucketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class S3BucketController {


    @Autowired
    BucketService bucketService;


    @PostMapping("/create_bucket/{name}")
    public String createS3Bucket(@PathVariable("name") String name) {

        return bucketService.createBucket(name);
    }


    @GetMapping("/bucket_list")
    public List<String> getBucketList() {
        return bucketService.getBucketList();
    }


    @DeleteMapping("/delete_bucket/{bucket}")
    public String deleteBucket(@PathVariable("bucket") String bucket) {
        return bucketService.deleteBucket(bucket);
    }


    @PutMapping("/file_upload_to_s3/{bucketName}")
    public String uploadFileToS3(@PathVariable("bucketName") String bucketName){
        return bucketService.fileUploadToS3(bucketName);
    }
}
