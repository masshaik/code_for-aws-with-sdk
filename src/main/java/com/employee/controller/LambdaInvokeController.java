package com.employee.controller;

import com.employee.entity.EmployeeAndStudent;
import com.employee.service.LambdaInvokeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LambdaInvokeController {


    @Autowired
    private LambdaInvokeService lambdaInvokeService;

    @GetMapping("/get_student_with_employee/{id}")
    public EmployeeAndStudent getStudentDetailsAlongWithStudentInformation(@PathVariable("id") String name) throws JsonProcessingException {
        return lambdaInvokeService.getEmployeeAndStudentInfo(name);

    }
}
