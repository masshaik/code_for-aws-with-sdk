package com.employee.controller;

import com.employee.entity.Department;
import com.employee.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RDSMySQLController {


    @Autowired
    DepartmentService departmentService;

    @PostMapping("/create_department")
    public ResponseEntity<Department> createDepartment(@RequestBody Department department) {
        return new ResponseEntity(departmentService.createDepartment(department), HttpStatus.CREATED);
    }


    @GetMapping("/get_department/{departmentId}")
    public ResponseEntity<Department> getDepartment(@PathVariable("departmentId") int departmentId) {
        return new ResponseEntity(departmentService.getDepartment(departmentId), HttpStatus.CREATED);
    }
}
