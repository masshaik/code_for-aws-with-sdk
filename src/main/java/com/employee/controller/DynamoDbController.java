package com.employee.controller;

import com.employee.entity.Employee;
import com.employee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DynamoDbController {


    @Autowired
    EmployeeRepository employeeRepository;

    @PostMapping("/create_employee")
    public Employee createEmployee(@RequestBody Employee employee) {

        return employeeRepository.createEmployee(employee);

    }


    @GetMapping("/get_employee/{employeeId}")
    public Employee getEmployee(@PathVariable("employeeId") String employeeId) {
        return employeeRepository.loadEmployee(employeeId);
    }
}
