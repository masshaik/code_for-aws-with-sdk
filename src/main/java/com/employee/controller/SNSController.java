package com.employee.controller;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SNSController {


    @Autowired
    private AmazonSNSClient amazonSNSClient;

    private String topicName = "arn:aws:sns:us-east-1:026362841459:shaik-queue";

    @GetMapping("/add_subscription/{email}")
    public String addSubscription(@PathVariable("email") String email){
        SubscribeRequest subscribeRequest = new SubscribeRequest(topicName,"email",email);
        amazonSNSClient.subscribe(subscribeRequest);
        return "Subsription Request is pending.... check your email for the confirmation:"+email;
    }


    @GetMapping("/publish_message/{message}")
    public String publishMessage(@PathVariable("message") String message){

        PublishRequest publishRequest = new PublishRequest(topicName,"Servers are getting down so developers please keep on checking for the availability of the servers","Notification Regarding the Cricket");
        amazonSNSClient.publish(publishRequest);
        return "Notification sent successfully....";
    }
}
