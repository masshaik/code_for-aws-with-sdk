package com.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SQSLearnController {


    @Autowired
    public QueueMessagingTemplate queueMessagingTemplate;


    @Value(value = "${queueEndpoint}")
    private String endPoint;


    @GetMapping("/send_message/{message}")
    public String sendMessageToQueue(@PathVariable("message") String message) {

        if(queueMessagingTemplate!=null){
            System.out.println("Object has been created");
        }

        queueMessagingTemplate.send(endPoint, MessageBuilder.withPayload(message).build());
        return "Message Sent";
    }
}
