package com.employee.service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.Region;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BucketService {


    @Value(value = "${awsAccessKey}")
    private String awsAccessKey;
    @Value(value = "${awsSecretAccessKey}")
    private String awsSecretAccessKey;


    public String createBucket(String bucketName) {
        AmazonS3 amazonS3 = getAmazonS3();
        Bucket bucket = null;


        if (amazonS3.doesBucketExist(bucketName)) {
            throw new RuntimeException("Sorry Bucket Already Exists with that name.... Try again with the different name please....");
        } else {
            bucket = amazonS3.createBucket(bucketName);
        }

        return bucket.getName() + ".... Has been successfully created";

    }

    public List<String> getBucketList() {

        AmazonS3 amazonS3 = getAmazonS3();
        List<Bucket> buckets = amazonS3.listBuckets();
        List<String> list = new ArrayList<>();
        for (Bucket bucket : buckets) {
            list.add(bucket.getName());
        }
        return list;
    }


    public AmazonS3 getAmazonS3() {
        Region region = Region.AP_Mumbai;

        AWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretAccessKey);

        AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).withRegion(String.valueOf(region)).build();
        return amazonS3;
    }

    public String deleteBucket(String bucketName) {

        AmazonS3 amazonS3 = getAmazonS3();
        amazonS3.deleteBucket(bucketName);
        return "succcesfully deleted";
    }

    public String fileUploadToS3(String bucketName) {
        //String bucketName = "learn-s3-shaik";

        com.amazonaws.regions.Region region = com.amazonaws.regions.Region.getRegion(Regions.US_WEST_1);
        AWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretAccessKey);


        System.out.println("Value of Credentials are:"+awsAccessKey+"...."+awsSecretAccessKey);

        AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).withRegion(String.valueOf(region)).build();


        PutObjectResult putObjectResult = amazonS3.putObject(bucketName, "Documents/pom.xml", "file:///Users/masshaik/Documents/pom.xml");


        return "Object has been stored successfully";
    }
}
