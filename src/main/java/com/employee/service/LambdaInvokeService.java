package com.employee.service;

import com.employee.aws.integration.AwsLambdaInvokeFunctionality;
import com.employee.entity.Employee;
import com.employee.entity.EmployeeAndStudent;
import com.employee.repository.EmployeeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class LambdaInvokeService {


    @Autowired
    AwsLambdaInvokeFunctionality awsLambdaInvokeFunctionality;

    @Autowired
    EmployeeRepository employeeRepository;

    public EmployeeAndStudent getEmployeeAndStudentInfo(String name) throws JsonProcessingException {

        EmployeeAndStudent employeeAndStudent = new EmployeeAndStudent();
        String studentObject = awsLambdaInvokeFunctionality.invokeAwsLambda("functionName", employeeAndStudent);
        Employee employee = employeeRepository.loadEmployee(name);

        employeeAndStudent.setEmployee(employee);
        return employeeAndStudent;
    }


}
